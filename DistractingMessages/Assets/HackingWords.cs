﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Specialized;
using System.IO;


public class HackingWords {

    public static StringCollection mostCommonWords = new StringCollection();

    public static StringCollection AllWords
    {
        get
        {
            if (mostCommonWords.Count < 1)
            {
                TextAsset wordsData = Resources.Load<TextAsset>("1000Words");
                var allTheWords = wordsData.ToString().Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var word in allTheWords)
                {
                    mostCommonWords.Add(word);
                }
            }
            return mostCommonWords;
        }
    }

    public static string GetRandomWord()
    {
        return AllWords[UnityEngine.Random.Range(0, AllWords.Count)];
    }
}
