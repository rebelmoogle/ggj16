﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinScreen : MonoBehaviour {

    private TempSaveValues tempSaveValues;

    // Use this for initialization
    void Start () {
        var tempValues = GameObject.Find("TempStaticValues");
        if (tempValues != null)
        {
            tempSaveValues = tempValues.GetComponent<TempSaveValues>();
            GetComponent<Text>().text += " " + tempSaveValues.GetValue("time");
        }


    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
