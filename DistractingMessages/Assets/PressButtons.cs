﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PressButtons : MonoBehaviour
{

    private TempSaveValues tempSaveValues;

    void Awake()
    {
        var tempValues = GameObject.Find("TempStaticValues");
        if (tempValues == null)
        {
            tempValues = Instantiate(Resources.Load<GameObject>("Prefabs/TempStaticValues"));
            tempValues.name = "TempStaticValues";
        }

        tempSaveValues = tempValues.GetComponent<TempSaveValues>();
    }

    public void LoadLevel()
    {
        var levelName = tempSaveValues.GetValue("LevelToLoad");
        if (levelName != null)
        {
            SceneManager.LoadScene(levelName);
        }
    }

    public void LoadLevel(string levelName)
    {

            SceneManager.LoadScene(levelName);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
