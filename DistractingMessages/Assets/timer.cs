﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class timer : MonoBehaviour
{
    private float StartTime;

    [SerializeField]
    private float HackTime; // amount of time in seconds player has for the level

    delegate void TimeOutEvent();
    private static event TimeOutEvent OnTimeOut;

    private TempSaveValues tempSaveValues;



	// Use this for initialization
	void Start ()
	{
        StartTime = Time.time;
	    var tempValues = GameObject.Find("TempStaticValues");
	    if (tempValues == null)
	    {
	        tempValues = Instantiate(Resources.Load<GameObject>("Prefabs/TempStaticValues"));
	        tempValues.name = "TempStaticValues";
	    }

	    tempSaveValues = tempValues.GetComponent<TempSaveValues>();
	}
	
	// Update is called once per frame
	void Update ()
	{

	    float elapsedTime = Time.time - StartTime;
	    Text textComp = this.GetComponent<Text>();
	    

	    if (elapsedTime > HackTime)
	    {
            if (textComp)
            {
                textComp.text = "GAME OVER!";
                //TODO turn this into an event and load a menu

                tempSaveValues.AddValue("LevelToLoad", SceneManager.GetActiveScene().name);
                SceneManager.LoadScene("GameOver");
            }
            if (OnTimeOut != null)
	        {
	            OnTimeOut();
	        }

	    }
        else if (textComp)
        {
            textComp.text = (HackTime - elapsedTime).ToString();
        }
    }
}
