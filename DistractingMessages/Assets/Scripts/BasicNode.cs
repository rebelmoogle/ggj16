﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Rendering;
using UnityEngine.UI;

enum NodeType
{
    StartNode,
    ConnectorNode,
    DataNode,
    Mainframe
}

enum NodeStatus
{
    INACCESSIBLE,
    ACCESSIBLE,
    HACKED
}

public class BasicNode : MonoBehaviour
{
    [SerializeField]
    private NodeType SelectedType;

    [SerializeField]
    private GameObject HackingPrefab;

    [SerializeField]
    private List<BasicNode> connectedNodes = new List<BasicNode>();

    private NodeStatus currentStatus = NodeStatus.INACCESSIBLE;

    private Canvas mainCanvas;

    private GameMode curGameMode;

    [SerializeField]
    private bool NeedHackToWin = false;

    private SpriteRenderer curSprite;

    private Color accessColor = Color.blue;
    private Color hackedColor = Color.cyan;

    void OnValidate()
    {
        var curSprite = this.GetComponent<SpriteRenderer>();
        if (!curSprite)
        {
            curSprite = gameObject.AddComponent<SpriteRenderer>();
        }
        curSprite.sortingOrder = 1;
        curSprite.color = Color.gray;

        switch (SelectedType)
        {
            case NodeType.StartNode:
                curSprite.sprite = Resources.Load<Sprite>("Graphics/StartNode");
                currentStatus = NodeStatus.ACCESSIBLE;
                curSprite.color = accessColor;
                break;
            case NodeType.ConnectorNode:
                curSprite.sprite = Resources.Load<Sprite>("Graphics/ConnectorNode");
                break;
            case NodeType.DataNode:
                curSprite.sprite = Resources.Load<Sprite>("Graphics/DataNode");
                break;
            case NodeType.Mainframe:
                curSprite.sprite = Resources.Load<Sprite>("Graphics/Mainframe");
                break;
            default:
                curSprite.sprite = Resources.Load<Sprite>("Graphics/DataNode");
                break;
        }

    }

    // Use this for initialization
    void Start()
    {

        mainCanvas = FindObjectOfType<Canvas>();
        if (mainCanvas == null)
        {
            mainCanvas = new Canvas();
        }

        var tempGameModeObj = GameObject.Find("GameMode");
        if (tempGameModeObj == null)
        {
            tempGameModeObj = Instantiate(Resources.Load<GameObject>("GameMode"));
        }
        curGameMode = tempGameModeObj.GetComponent<GameMode>();

        if (NeedHackToWin)
        {
            curGameMode.AddWinCondition();
            accessColor = Color.red;
            hackedColor = Color.green;
        }

        if (curSprite == null)
        {
            curSprite = GetComponent<SpriteRenderer>();
        }

        // load graphic depending on selected node.
        // connect to other nodes.
        foreach (var curNode in connectedNodes)
        {
            // load and instantiate connector graphics
            var curConnector = new GameObject();
            curConnector.transform.position = transform.position;
            curConnector.transform.parent = transform;
            curConnector.layer = 9;
            var myLine = curConnector.AddComponent<LineRenderer>();
            myLine.material = Resources.Load<Material>("Materials/ConnectorLineMat");

            myLine.receiveShadows = false;
            myLine.shadowCastingMode = ShadowCastingMode.Off;
            myLine.useLightProbes = false;
            myLine.SetVertexCount(2);
            Vector3 direction = transform.position - curNode.transform.position;
            var directionAngleRight = Vector3.Dot(direction.normalized, Vector3.right);
            var directionAngleUp = Vector3.Dot(direction.normalized, Vector3.up);

            myLine.SetPosition(0, transform.position + directionAngleUp * Vector3.right * 0.1f + directionAngleRight * Vector3.up * 0.1f); // up or down on y-axis, dot: -1 --> other direction, dot: 0 --> right angle, dot: 1: same direction.
            myLine.SetPosition(1, curNode.transform.position + directionAngleUp * Vector3.right * 0.1f + directionAngleRight * Vector3.up * 0.1f); myLine.SetWidth(0.1f, 0.1f);
            myLine.SetColors(Color.gray, Color.black); // default is not accessible.




        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    // script is loaded or value is changed in editor.
    void OnDrawGizmos()
    {
        foreach (var curNode in connectedNodes)
        {
            // draw the connection in editor
            Gizmos.DrawLine(transform.position, curNode.transform.position);
        }
    }

    public void NodeHacked()
    {
        // call if node was successfully hacked. 

        currentStatus = NodeStatus.HACKED;
        curSprite.color = hackedColor;


        // highlight accessible connections
        var connectorLines = GetComponentsInChildren<LineRenderer>();
        foreach (var curLine in connectorLines)
        {
            curLine.SetColors(hackedColor, Color.blue);
        }

        //hacky way of changing color. :P
        foreach (var curNode in connectedNodes)
        {
            if (curNode.currentStatus == NodeStatus.INACCESSIBLE)
            {
                curNode.GetComponent<SpriteRenderer>().color = curNode.accessColor;
                curNode.currentStatus = NodeStatus.ACCESSIBLE;
            }
        }

        if (NeedHackToWin)
        {
            // win condition
            curGameMode.RemoveWinCondition();
        }
    }

    void OnMouseEnter()
    {
        var hackboxpresent = mainCanvas.GetComponentInChildren<HackingBox>();
        if (hackboxpresent != null)
        {
            return; // HackMessageBox already present.
        }
        

        if (curSprite && currentStatus == NodeStatus.ACCESSIBLE)
        {
            curSprite.color = Color.white;
        }
    }

    void OnMouseExit()
    {
        if (curSprite && curSprite.color == Color.white)
        {
            switch (currentStatus)
            {
                case NodeStatus.INACCESSIBLE:
                    curSprite.color = Color.gray;
                    break;
                case NodeStatus.ACCESSIBLE:
                    curSprite.color = accessColor;
                    break;
                case NodeStatus.HACKED:
                     curSprite.color = hackedColor;
                    break;
                default:
                    break;
            }
            
        }
    }

    void OnMouseDown()
    {
        var hackboxpresent = mainCanvas.GetComponentInChildren<HackingBox>();
        if (hackboxpresent != null)
        {
            return; // HackMessageBox already present.
        }

        if (currentStatus == NodeStatus.ACCESSIBLE)
        {
            if (!HackingPrefab)
            {
                Debug.LogError("NO HACKING PREFAB PRESENT. Setting Node to hacked.");
                NodeHacked();
            }
            else
            {
                    var HackingClone = Instantiate(HackingPrefab);
                    HackingClone.transform.parent = mainCanvas.transform;
                    HackingClone.transform.position = new Vector3(652, 187, 0);
                    HackingClone.GetComponentInChildren<InputField>().Select();
                    var hackBox = HackingClone.GetComponentInChildren<HackingBox>();
                    hackBox.OnHacked += NodeHacked;
            }
        }

    }
}
