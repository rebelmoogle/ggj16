﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StressingSoundSource : MonoBehaviour {

    //General
    AudioClipController myAudioClipController;

    public AudioClip clip;
    public double soundRepeatTime = 1.0;
    public bool active = false;

    double nextSoundPlaytime;
    double schedulingMargin = 0.1; //Give the system some time to arrange the scheduling. This is important to get right. A smaller figure such as 0.1 seems to produce best results. In this implementation, when this gets too large, the sound will keep repeating after the firing key is released. With a too small value, there appears arrhythmia.

    void Start()
    {
        //General
        myAudioClipController = GetComponent<AudioClipController>();

        nextSoundPlaytime = AudioSettings.dspTime + soundRepeatTime;
    }

    void Update()
    {
        double time = AudioSettings.dspTime;

        if (active)
        {
            if (nextSoundPlaytime <= 0.01)
            {
                nextSoundPlaytime = time + soundRepeatTime;
            }

            if (time > nextSoundPlaytime - schedulingMargin)
            {
                myAudioClipController.PlayClipScheduled(clip, nextSoundPlaytime, 8);
                nextSoundPlaytime += soundRepeatTime;
            }
        }
        else
        {
            nextSoundPlaytime = 0;
        }

    }
}
