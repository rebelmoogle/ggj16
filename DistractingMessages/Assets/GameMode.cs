﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameMode : MonoBehaviour
{
    [SerializeField]
    private string NextLevelName = "MainMenu";

    private int numWinConditions = 0;
    private bool isSet = false;

    private TempSaveValues tempSaveValues;

    // Use this for initialization
    void Start () {
        var tempValues = GameObject.Find("TempStaticValues");
        if (tempValues == null)
        {
            tempValues = Instantiate(Resources.Load<GameObject>("Prefabs/TempStaticValues"));
            tempValues.name = "TempStaticValues";
        }

        tempSaveValues = tempValues.GetComponent<TempSaveValues>();
    }
	
	// Update is called once per frame
	void Update () {
	    if (numWinConditions <= 0)
	    {
	        tempSaveValues.AddValue("LevelToLoad", NextLevelName);
            tempSaveValues.AddValue("time", Time.timeSinceLevelLoad.ToString());
            SceneManager.LoadScene("Win");;
	    }
	}

   public void AddWinCondition()
    {
        numWinConditions += 1;
        isSet = true;
    }

    public void RemoveWinCondition()
    {
        numWinConditions -= 1;
    }
}
