﻿using UnityEngine;
using System.Collections.Generic;

public class TempSaveValues : MonoBehaviour
{
    private static Dictionary<string, string> tempValues = new Dictionary<string, string>();

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void AddValue(string key, string value)
    {
        tempValues.Add(key, value);
    }

    public string GetValue(string key)
    {
        return tempValues[key];
    }
}
