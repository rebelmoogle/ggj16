Speed Racer (II) by Vir Nocturna is licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 License
http://freemusicarchive.org/music/Vir_Nocturna/Speed_Racer_2_-_Single/speedracer2

Lost Cities by Alexandre Navarro is licensed under a Attribution-NonCommercial-ShareAlike License
http://freemusicarchive.org/music/Alexandre_Navarro/Lost_Cities/Alexandre_Navarro_-_Lost_Cities_-_01_Lost_Cities

Nubuck by Michett is licensed under a Attribution 3.0 International License
http://freemusicarchive.org/music/Michett/~/Nubuck