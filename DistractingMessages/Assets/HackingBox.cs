﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HackingBox : MonoBehaviour
{
    private Text textDisplay;
    private InputField inputDisplay;

    public Action OnHacked;

	// Use this for initialization
	void Start ()
	{
	    textDisplay = GetComponentInChildren<Text>();
	    inputDisplay = GetComponentInChildren<InputField>();
        // display the words that need to be typed. 

	    textDisplay.text = HackingWords.GetRandomWord();
	    textDisplay.text += " " + HackingWords.GetRandomWord();
        textDisplay.text += " " + HackingWords.GetRandomWord();

    }
	
	// Update is called once per frame
	void Update () {
	    // highlight words
    }

    // function for checking the correct words have been typed. (On End)
    public void HandleHackingInput(string inputText)
    {
        if (inputText != textDisplay.text)
        {

                inputDisplay.text = "INCORRECT ENTRY! ACCESS DENIED! (try again.)";
                inputDisplay.ActivateInputField();
                inputDisplay.Select();
                return;
        }
        else
        {
            inputDisplay.text = "ACCESS GRANTED!";
            if (OnHacked != null)
            { OnHacked(); }

            Destroy(this.gameObject);
        }

    }
    // close window if done, go back to game, mark node as hacked?
    // do not let player interact with nodes as long as hacking is in progress.
}
