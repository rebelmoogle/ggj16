# The Distraction / A Global Game Jam 2016 Prototype

## The Team

John McLaughlin 

Kaylin Norman-Slack 

Robert Bergner 

## Made With

* Unity
* Inkscape
* Windows
* and Love.

## Sounds

These sound are CC licenced and where retrieved from these URLs:

* http://freesound.org/people/Greencouch/sounds/124900/
* http://freesound.org/people/KeyKrusher/sounds/154953/
* http://freesound.org/people/yunikon/sounds/333860/
* http://freesound.org/people/Empty%20Bell/sounds/180821/
* http://freesound.org/people/unfa/sounds/154894/

## Graphics

* Title background: https://www.flickr.com/photos/gi/128956889
* Access Granted background: https://www.flickr.com/photos/cvander/3004133900
* Access Denied background: https://www.flickr.com/photos/psd/10292377
* Credits background: https://i.ytimg.com/vi/F7oVwGIAYeU/maxresdefault.jpg
* Font: http://www.fontzone.net/font-details/aharoni-bold

## Music

Speed Racer (II) by Vir Nocturna is licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 License
http://freemusicarchive.org/music/Vir_Nocturna/Speed_Racer_2_-_Single/speedracer2

Lost Cities by Alexandre Navarro is licensed under a Attribution-NonCommercial-ShareAlike License
http://freemusicarchive.org/music/Alexandre_Navarro/Lost_Cities/Alexandre_Navarro_-_Lost_Cities_-_01_Lost_Cities

Nubuck by Michett is licensed under a Attribution 3.0 International License
http://freemusicarchive.org/music/Michett/~/Nubuck